const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const passport = require('passport')

const { mongodbUri } = require('./config/keys')

const authRoutes = require('./routes/auth')
const teacherRoutes = require('./routes/teacher')
const facultyRoutes = require('./routes/faculty')
const groupRoutes = require('./routes/group')
const studentRoutes = require('./routes/student')

const app = express()

mongoose.connect(mongodbUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,

    useCreateIndex: true
})
    .then(() => console.log('MongoDB connected'))
    .catch(error => console.log(error))

app.use(passport.initialize()) // ! Вказуємо що express, буде працювати із паспортом
require('./middleware/passport')(passport)

/*
    * morgan - для логування запитів. Що відбувається із сервером в даний момент
*/
app.use(require('morgan')('dev'))
app.use('/assets', express.static('assets'))
/*
    * body-parser для роботи з req.body
    * bodyParser.json() для генерування json об'єктів  
*/
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

/*
    * cors - для відповіді на корс запити. Слугує для того, щоб відповідати на запити із інших доминів
*/
app.use(require('cors')())

app.use('/api/auth', authRoutes)
app.use('/api/teacher', teacherRoutes)
app.use('/api/faculty', facultyRoutes)
app.use('/api/group', groupRoutes)
app.use('/api/student', studentRoutes)


module.exports = app