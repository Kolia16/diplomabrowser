const bcrypt = require('bcryptjs')

const Student = require('../models/InfoStudent')
const User = require('../models/User')

const errorHandler = require('../utils/errorHandler')

module.exports.getStudentsGroup = async (req, res) => {
    try {
        const student = await Student.find({
            group: req.params.id
        })

        res.status(200).json(student)
    } catch (e) {
        errorHandler(res, e)
    }
}

module.exports.getById = async (req, res) => {
    try {
        const student = await Student.findById(req.params.id)
        res.status(200).json(student)
    } catch (e) {
        errorHandler(res, e)
    }
}

module.exports.create = async (req, res) => {
    try {
        const salt = bcrypt.genSaltSync(10)
        const password = req.body.password

        const user = await new User({
            login: req.body.login,
            password: bcrypt.hashSync(password, salt),
            status: req.body.status
        }).save()

        const student = await new Student({
            name: req.body.name,
            patronymic: req.body.patronymic,
            surname: req.body.surname,
            group: req.body.group,
            user: user._id
        }).save()

        res.status(201).json(student)
    } catch (e) {
        errorHandler(res, e)
    }
}

module.exports.update = async (req, res) => {
    try {
        const student = await Student.findOneAndUpdate(
            { user: req.params.id },
            { $set: req.body },
            { new: true }
        )
        res.status(200).json(student)
    } catch (e) {
        errorHandler(res, e)
    }
}

module.exports.remove = async (req, res) => {
    try {
        await Student.remove({ user: req.params.id })
        await User.remove({ _id: req.params.id })
        res.status(200).json({
            message: 'Студент успiшно видалений'
        })
    } catch (e) {
        errorHandler(res, e)
    }
}