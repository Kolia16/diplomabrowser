const bcrypt = require('bcryptjs')

const Teacher = require('../models/InfoTeacher')
const User = require('../models/User')

const errorHandler = require('../utils/errorHandler')

module.exports.getAll = async (req, res) => {
    try {
        const teachers = await Teacher.find()
        res.status(200).json(teachers)
    } catch (e) {
        errorHandler(res, e)
    }
}

module.exports.getById = async (req, res) => {
    try {
        const teacher = await Teacher.findById(req.params.id)
        res.status(200).json(teacher)
    } catch (e) {
        errorHandler(res, e)
    }
}

module.exports.create = async (req, res) => {
    try {
        const salt = bcrypt.genSaltSync(10)
        const password = req.body.password

        const user = await new User({
            login: req.body.login,
            password: bcrypt.hashSync(password, salt),
            status: req.body.status
        }).save()

        const teacher = await new Teacher({
            name: req.body.name,
            patronymic: req.body.patronymic,
            surname: req.body.surname,
            faculty: req.body.faculty,
            subject: req.body.subject,
            user: user._id
        }).save()

        res.status(201).json(teacher)
    } catch (e) {
        errorHandler(res, e)
    }
}

module.exports.update = async (req, res) => {
    try {
        const teacher = await Teacher.findOneAndUpdate(
            { user: req.params.id },
            { $set: req.body },
            { new: true }
        )
        res.status(200).json(teacher)
    } catch (e) {
        errorHandler(res, e)
    }
}

module.exports.remove = async (req, res) => {
    try {
        await Teacher.remove({ user: req.params.id })
        await User.remove({ _id: req.params.id })
        res.status(200).json({
            message: 'Викладач успiшно видалений'
        })
    } catch (e) {
        errorHandler(res, e)
    }
}

