const Faculty = require('../models/Faculty')
const errorHandler = require('../utils/errorHandler')

module.exports.getAll = async (req, res) => {
    try {
        const faculty = await Faculty.find()
        res.status(200).json(faculty)
    } catch (e) {
        errorHandler(res, e)
    }
}

module.exports.getById = async (req, res) => {
    try {
        const faculty = await Faculty.findById(req.params.id)
        res.status(200).json(faculty)
    } catch (e) {
        errorHandler(res, e)
    }
}

module.exports.create = async (req, res) => {
    try {
        const faculty = await new Faculty({
            name_faculty: req.body.name_faculty
        }).save()
        res.status(201).json(faculty)
    } catch (e) {
        errorHandler(res, e)
    }
}

module.exports.update = async (req, res) => {
    try {
        const faculty = await Faculty.findOneAndUpdate(
            { _id: req.params.id },
            { $set: req.body },
            { new: true }
        )
        res.status(200).json(faculty)
    } catch (e) {
        errorHandler(res, e)
    }
}

module.exports.remove = async (req, res) => {
    try {
        await Faculty.remove({ _id: req.params.id })
        res.status(200).json({
            message: 'Факультет успiшно видалений'
        })
    } catch (e) {
        errorHandler(res, e)
    }
}