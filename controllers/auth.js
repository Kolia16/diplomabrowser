/*
    * bcrypt - Для шифрування пароля
*/
const bcrypt = require('bcryptjs')
/*
    *jwt - Для генерації токена
*/
const jwt = require('jsonwebtoken')

const keys = require('../config/keys')

const errorHandler = require('../utils/errorHandler')

const User = require('../models/User')

module.exports.login = async (req, res) => {
    const candidate = await User.findOne({ login: req.body.login })


    if (candidate) {
        // Користувач існує
        const passwordResult = bcrypt.compareSync(req.body.password, candidate.password)

        if (passwordResult) {
            // Генерація токіна, пароль підійшов
            const token = jwt.sign({
                login: candidate.login,
                userId: candidate._id
            }, keys.jwt, { expiresIn: 60 * 60 }) // * expiresIn - час дійснострі токена

            res.status(200).json({
                token: `Bearer ${token}`
            })
        } else {
            // Паролі не збігаються
            res.status(401).json({
                message: 'Паролі не збігаються'
            })
        }
    } else {
        // Такого користувача не існує
        res.status(404).json({
            message: 'Користувача із таким email не знайден'
        })
    }
}


module.exports.register = async (req, res) => {
    // login, password
    const candidate = await User.findOne({ login: req.body.login })

    if (candidate) {
        // Користувач існує
        res.status(409).json({
            message: 'Такий вже зайнятий. Спробуйте обрати інший!'
        })
    } else {
        // Користувача немає 
        const salt = bcrypt.genSaltSync(10)
        const password = req.body.password

        const user = new User({
            login: req.body.login,
            password: bcrypt.hashSync(password, salt),
            status: req.body.status
        })

        try {
            await user.save()
            res.status(201).json(user)
        } catch (e) {
            // Опрацювати помилку

            errorHandler(res, e)
        }
    }
}