const Group = require('../models/Group')

const errorHandler = require('../utils/errorHandler')

module.exports.get = async (req, res) => {
    try {
        const group = await Group.find()
        res.status(200).json(group)
    } catch (e) {
        errorHandler(res, e)
    }
}

module.exports.getGroupsOnFaculty = async (req, res) => {
    try {
        const group = await Group.find({
            faculty: req.params.id
        })
        res.status(200).json(group)
    } catch (e) {
        errorHandler(res, e)
    }
}

module.exports.getById = async (req, res) => {
    try {
        const group = await Group.findById(req.params.id)
        res.status(200).json(group)
    } catch (e) {
        errorHandler(res, e)
    }
}

module.exports.create = async (req, res) => {
    try {
        const group = await new Group({
            name_course: req.body.name_course,
            course: req.body.course,
            faculty: req.body.faculty
        }).save()
        res.status(201).json(group)
    } catch (e) {
        errorHandler(res, e)
    }
}

module.exports.update = async (req, res) => {
    try {
        const group = await Group.findOneAndUpdate(
            { _id: req.params.id },
            { $set: req.body },
            { new: true }
        )
        res.status(200).json(group)
    } catch (e) {
        errorHandler(res, e)
    }
}

module.exports.remove = async (req, res) => {
    try {
        await Group.remove({ _id: req.params.id })
        res.status(200).json({
            message: 'Група успiшно видалений'
        })
    } catch (e) {
        errorHandler(res, e)
    }
}