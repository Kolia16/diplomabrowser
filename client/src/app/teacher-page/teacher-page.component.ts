import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';


@Component({
  selector: 'app-teacher-page',
  templateUrl: './teacher-page.component.html',
  styleUrls: ['./teacher-page.component.sass']
})
export class TeacherPageComponent implements OnInit {

  ELEMENT_DATA: PeriodicElement[] = [
    { surname_name_patronymic: 'Baranovsky Kolia Kolia', faculty: 'FIKT', user: 'kb45tbrtddt5yby6b6ydbb5rb5' },
    { surname_name_patronymic: 'Baranovsky Kolia Kolia1', faculty: 'FIKT', user: 'kb45tbrtddt5yby6b6ydbb5rb5' },
    { surname_name_patronymic: 'Baranovsky Kolia Kolia2', faculty: 'FIKT', user: 'kb45tbrtddt5yby6b6ydbb5rb5' },
    { surname_name_patronymic: 'Baranovsky Kolia Kolia2', faculty: 'AUTO', user: 'kb45tbrtddt5yby6b6ydbb5rb5' },
    { surname_name_patronymic: 'Baranovsky Kolia Kolia3', faculty: 'FIKT', user: 'kb45tbrtddt5yby6b6ydbb5rb5' },
    { surname_name_patronymic: 'Baranovsky Kolia Kolia33', faculty: 'FIKT', user: 'kb45tbrtddt5yby6b6ydbb5rb5' },
    { surname_name_patronymic: 'Baranovsky Kolia Kolia3', faculty: 'AUTO', user: 'kb45tbrtddt5yby6b6ydbb5rb5' },
    { surname_name_patronymic: 'Baranovsky Kolia Kolia3', faculty: 'FIKT', user: 'kb45tbrtddt5yby6b6ydbb5rb5' },
    { surname_name_patronymic: 'Baranovsky Kolia Kolia45', faculty: 'FIKT', user: 'kb45tbrtddt5yby6b6ydbb5rb5' },
  ];

  displayedColumns: string[] = ['surname_name_patronymic', 'faculty', 'buttons'];
  dataSource: MatTableDataSource<PeriodicElement>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor() {

    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;


  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;

    this.dataSource.filter = filterValue.trim().toLowerCase();


    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}


export interface PeriodicElement {
  surname_name_patronymic: string;
  faculty: string;
  user: string;
}
