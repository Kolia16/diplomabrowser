import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MaterialInstanse, MaterialService, Chips } from 'src/app/shared/classes/material.service';



@Component({
  selector: 'app-create-teacher',
  templateUrl: './create-teacher.component.html',
  styleUrls: ['./create-teacher.component.sass']
})
export class CreateTeacherComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('select') selectRef: ElementRef
  @ViewChild('chips') chipsRef: ElementRef

  form: FormGroup

  select: MaterialInstanse
  chips: Chips





  constructor() {
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      surname: new FormControl(null, [Validators.required]),
      patronymic: new FormControl(null, [Validators.required]),
      login: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required]),
      faculty: new FormControl(null, [Validators.required]),
      subject: new FormControl(null, [])
    })


  }



  onSubmit() {
    console.log(this.form)
    console.log(this.form.value.faculty)
  }

  ngAfterViewInit() {
    this.select = MaterialService.initFormSelect(this.selectRef)
    this.chips = MaterialService.initChip(this.chipsRef)

  }

  ngOnDestroy() {
    this.select.destroy()
  }

  xx() {
    console.log(MaterialService.getInstanceChip(this.chipsRef))
  }


}
