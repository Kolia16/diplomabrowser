
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.sass']
})
export class AdminLayoutComponent implements OnInit {

  gpoups_links = [
    {
      gpoup_name: 'Користувачи',
      links: [
        { url: 'teacher', name: 'Викладачи', icon: 'folder_shared' },
        { url: 'student', name: 'Студенти', icon: 'group' }
      ]
    },

  ]

  @ViewChild('buttonMenu') buttonMenuRef: ElementRef | any
  @ViewChild('drawer') drawerRef: ElementRef | any

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  logout(event: Event) {
    // ToDo Для відміни стандартної поведінки силки
    event.preventDefault()

    this.auth.logout()
    this.router.navigate(['/login'])
  }


}
