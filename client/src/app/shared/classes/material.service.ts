import { ElementRef } from '@angular/core'

declare var M

export interface MaterialInstanse {
  open?(): void,
  close?(): void,
  destroy?(): void,
}

export interface Chips {
  addChip?(): void,
  deleteChip?(): void,
  selectChip?(): void,
  chipsData?
}


export class MaterialService {
  static toast(message: string) {
    M.toast({ html: message })
  }

  static initSidenav(ref: ElementRef) {
    return M.Sidenav.init(ref.nativeElement)
  }

  static initFormSelect(ref: ElementRef) {
    return M.FormSelect.init(ref.nativeElement)
  }

  static initModal(ref: ElementRef) {
    return M.Modal.init(ref.nativeElement)
  }

  static initChip(ref: ElementRef) {
    return M.Chips.init(ref.nativeElement)
  }

  static getInstanceChip (ref: ElementRef) {
    return M.Chips.getInstance(ref.nativeElement)
  }
}

