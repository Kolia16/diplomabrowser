export interface User {
  login: string
  password: string
}

export interface Teacher {
  _id?: String,
  login?: String,
  name: String,
  patronymic: String,
  surname: String,
  faculty: String,
  user?: String,
  subject?: Subject[]
}

export interface Subject {
  _id?: String,
  name: String
}
