import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TokenInterceptor } from './shared/classes/token.interceptor';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { AuthLayoutComponent } from './shared/layouts/auth-layout/auth-layout.component';
import { SiteLayoutComponent } from './shared/layouts/site-layout/site-layout.component';
import { MainPageComponent } from './main-page/main-page.component';
import { TeacherLayoutComponent } from './shared/layouts/teacher-layout/teacher-layout.component';
import { AdminLayoutComponent } from './shared/layouts/admin-layout/admin-layout.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TeacherPageComponent } from './teacher-page/teacher-page.component';
import { StudentPageComponent } from './student-page/student-page.component';
import { CreateTeacherComponent } from './teacher-page/modals/create-teacher/create-teacher.component';
import { GetTeacherComponent } from './teacher-page/modals/get-teacher/get-teacher.component';
import { UpdateTeacherComponent } from './teacher-page/modals/update-teacher/update-teacher.component';
import { RemoveTeacherComponent } from './teacher-page/modals/remove-teacher/remove-teacher.component';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSelectModule } from '@angular/material/select';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';



@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    AuthLayoutComponent,
    SiteLayoutComponent,
    MainPageComponent,
    TeacherLayoutComponent,
    AdminLayoutComponent,
    TeacherPageComponent,
    StudentPageComponent,
    CreateTeacherComponent,
    GetTeacherComponent,
    UpdateTeacherComponent,
    RemoveTeacherComponent,

  ],
  imports: [
    MatCardModule,
    MatFormFieldModule,
    MatIconModule,
    MatGridListModule,
    MatButtonModule,
    MatInputModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatSelectModule,
    MatDividerModule,
    MatListModule,
    MatToolbarModule,
    MatTableModule,
    MatPaginatorModule,

    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,

  ],
  providers: [{ // ! Реєструємо інтерсептор для запису токена в хедер
    provide: HTTP_INTERCEPTORS,
    multi: true,
    useClass: TokenInterceptor
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
