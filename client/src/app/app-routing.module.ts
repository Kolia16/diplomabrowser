import { AuthGuard } from './shared/classes/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthLayoutComponent } from './shared/layouts/auth-layout/auth-layout.component';
import { SiteLayoutComponent } from './shared/layouts/site-layout/site-layout.component';
import { TeacherLayoutComponent } from './shared/layouts/teacher-layout/teacher-layout.component';
import { AdminLayoutComponent } from './shared/layouts/admin-layout/admin-layout.component';

import { LoginPageComponent } from './login-page/login-page.component';
import { MainPageComponent } from './main-page/main-page.component';
import { TeacherPageComponent } from './teacher-page/teacher-page.component';
import { StudentPageComponent } from './student-page/student-page.component';


const routes: Routes = [
  {
    path: '', component: SiteLayoutComponent, children: [
      { path: '', redirectTo: '/main', pathMatch: 'full' },
      { path: 'main', component: MainPageComponent }
    ]
  },
  {
    path: 'teacher', component: TeacherLayoutComponent, children: [

    ]
  },
  {
    path: 'admin', component: AdminLayoutComponent, children: [
      {path: '', redirectTo: 'teacher', pathMatch: 'full'},
      {path: 'teacher', component: TeacherPageComponent},
      {path: 'student', component: StudentPageComponent}
    ]
  },
  {
    path: '', component: AuthLayoutComponent, children: [
      { path: 'login', component: LoginPageComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
