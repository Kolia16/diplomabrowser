const { Schema, model } = require('mongoose')

const groupSchema = new Schema({
    name_course: {
        type: String,
        required: true
    },
    course: {
        type: Number,
        required: true
    },
    faculty: {
        ref: 'faculties',
        type: Schema.Types.ObjectId
    }
})

module.exports = model('groups', groupSchema)