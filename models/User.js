const { Schema, model } = require('mongoose')

const userSchema = new Schema({
    login: {
        type: String,
        required: true, // * Обов'язковий
        unique: true // * Унікальний
    },
    password: {
        type: String,
        required: true,
    },
    status: {
        type: String,
        required: true
    }
})

module.exports = model('users', userSchema)
