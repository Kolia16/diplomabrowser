const { Schema, model } = require('mongoose')

const facultySchema = new Schema({
    name_faculty: {
        type: String,
        required: true
    }
})

module.exports = model('faculties', facultySchema)