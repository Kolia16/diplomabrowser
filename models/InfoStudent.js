const { Schema, model } = require('mongoose')

const infoStudentSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    patronymic: {
        type: String
    },
    surname: {
        type: String,
        required: true
    },
    group: {
        ref: 'groups',
        type: Schema.Types.ObjectId
    },
    user: {
        ref: 'users',
        type: Schema.Types.ObjectId
    }
})

module.exports = model('info_students', infoStudentSchema) 