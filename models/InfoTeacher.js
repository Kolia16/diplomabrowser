const { Schema, model } = require('mongoose')

const infoTeacherSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    patronymic: {
        type: String
    },
    surname: {
        type: String,
        required: true
    },
    faculty: {
        ref: 'faculties',
        type: Schema.Types.ObjectId
    },
    subject: [
        {
            name: {
                type: String
            }
        }
    ],
    user: {
        ref: 'users',
        type: Schema.Types.ObjectId
    }
})

module.exports = model('info_teachers', infoTeacherSchema)