const express = require('express')
const controller = require('../controllers/student')

const router = express.Router()

router.get('/group/:id', controller.getStudentsGroup)
router.get('/:id', controller.getById)
router.post('/', controller.create)
router.patch('/:id', controller.update)
router.delete('/:id', controller.remove)

module.exports = router